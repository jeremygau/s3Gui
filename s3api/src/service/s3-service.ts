import {
    type Bucket,
    CreateBucketCommand,
    type CreateBucketCommandInput,
    DeleteBucketCommand,
    type DeleteBucketCommandInput,
    DeleteObjectCommand,
    type DeleteObjectCommandInput,
    GetObjectCommand,
    type GetObjectCommandInput,
    ListBucketsCommand,
    type ListBucketsCommandInput,
    ListObjectsCommand,
    type ListObjectsCommandInput,
    PutObjectCommand,
    type PutObjectCommandInput,
    S3Client
} from "@aws-sdk/client-s3";

class S3Service {

    private s3: S3Client;

    constructor(s3Client: S3Client) {
        this.s3 = s3Client
    }

    async listBuckets(prefix: string = ""): Promise<Bucket[] | undefined> {
        const params: ListBucketsCommandInput = {
            Prefix: prefix,
        };

        try {
            const data = await this.s3.send(new ListBucketsCommand(params));
            return data.Buckets;
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async createBucket(bucketName: string) {
        const params: CreateBucketCommandInput = {
            Bucket: bucketName,
            CreateBucketConfiguration: {},
        };

        try {
            await this.s3.send(new CreateBucketCommand(params));
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async deleteBucket(bucketName: string) {
        const params: DeleteBucketCommandInput = {
            Bucket: bucketName,
        };

        try {
            await this.s3.send(new DeleteBucketCommand(params));
        } catch (err: any) {
            console.log(err, err.stack);
            throw err;
        }
    }

    async listObjects(bucketName: string, prefix: string = "") {
        const params: ListObjectsCommandInput = {
            Bucket: bucketName,
            Prefix: prefix,
        };

        try {
            const data = await this.s3.send(new ListObjectsCommand(params));
            return data.Contents ?? [];
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async addObject(bucketName: string, prefix: string, fileName: string, fileContent: Buffer) {
        const key = `${prefix}${fileName}`;
        const params: PutObjectCommandInput = {
            Bucket: bucketName,
            Key: key,
            Body: fileContent,
        };

        try {
            await this.s3.send(new PutObjectCommand(params));
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async addPrefix(bucketName: string, prefix: string) {
        const params: PutObjectCommandInput = {
            Bucket: bucketName,
            Key: prefix,
        };

        try {
            await this.s3.send(new PutObjectCommand(params));
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async deleteObject(bucketName: string, key: string) {
        const params: DeleteObjectCommandInput = {
            Bucket: bucketName,
            Key: key,
        };

        try {
            await this.s3.send(new DeleteObjectCommand(params));
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

    async downloadObject(bucketName: string, key: string) {
        const params: GetObjectCommandInput = {
            Bucket: bucketName,
            Key: key,
        };

        try {
            return await this.s3.send(new GetObjectCommand(params));
        } catch (err: any) {
            console.error(err, err.stack);
            throw err;
        }
    }

}

export default S3Service;