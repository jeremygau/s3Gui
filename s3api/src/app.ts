import {Hono} from 'hono'
import {cors} from 'hono/cors'
import {stream} from 'hono/streaming'
import S3Service from "./service/s3-service";
import {swaggerUI} from "@hono/swagger-ui";
import {serveStatic} from "hono/bun";

function verifyQueryParams(params: any, ...keys: string[]) {
    const errorMessages: { [key: string]: string } = {};
    keys.forEach((key) => {
        if (params[key] === undefined) {
            errorMessages[key] = `${key} is required`;
        }
    })
    return errorMessages;
}

function build(s3Service: S3Service) {

    const bucket = new Hono();

    bucket
        .get('/:bucketName', async (c) => {
            const {bucketName} = c.req.param();
            const {prefix} = c.req.query();
            try {
                return c.json(await s3Service.listObjects(bucketName, prefix ?? ''));
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .post('/:bucketName/create', async (c) => {
            const {bucketName} = c.req.param();
            try {
                await s3Service.createBucket(bucketName);
                c.status(201)
                return c.text('Bucket Created Successfully');
            } catch (e: any) {
                if (e.name === 'BucketAlreadyExists') {
                    c.status(409);
                    return c.text('Bucket Already Exists');
                }
                if (e.name === 'BucketAlreadyOwnedByYou') {
                    c.status(409);
                    return c.text('Bucket Already Owned By You');
                }
                if (e.name === 'InvalidBucketName') {
                    c.status(400);
                    return c.text('Invalid Bucket Name');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .delete('/:bucketName/delete', async (c) => {
            const {bucketName} = c.req.param();
            try {
                await s3Service.deleteBucket(bucketName);
                c.status(204);
                return c.text('Bucket Deleted Successfully');
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                if (e.name === 'BucketNotEmpty') {
                    c.status(409);
                    return c.text('Bucket Not Empty');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .post('/:bucketName/addObject', async (c) => {
            try {
                const {bucketName} = c.req.param();
                const {prefix, key} = c.req.query();
                const errorMessages = verifyQueryParams(c.req.query(), 'prefix', 'key');
                if (Object.keys(errorMessages).length > 0) {
                    c.status(400);
                    return c.json(errorMessages);
                }
                let objectContent;
                try {
                    objectContent = await c.req.formData();
                } catch (e) {
                    c.status(400);
                    return c.json({file: 'file is required'});
                }
                const blob = objectContent.get('file');
                if (blob === null || !(blob instanceof File)) {
                    c.status(400);
                    return c.json({file: 'file is required'});
                }
                await s3Service.addObject(bucketName, prefix, key, Buffer.from(await blob.arrayBuffer()));
                c.status(201)
                return c.text('Object Uploaded Successfully');
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .post('/:bucketName/addPrefix', async (c) => {
            try {
                const {bucketName} = c.req.param();
                const {prefix} = c.req.query();
                const errorMessages = verifyQueryParams(c.req.query(), 'prefix');
                if (Object.keys(errorMessages).length > 0) {
                    c.status(400);
                    return c.json(errorMessages);
                }
                await s3Service.addPrefix(bucketName, prefix);
                c.status(201)
                return c.text('Prefix Added Successfully');
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .delete('/:bucketName/deleteObject', async (c) => {
            const {bucketName} = c.req.param();
            const {key} = c.req.query();
            const errorMessages = verifyQueryParams(c.req.query(), 'key');
            if (Object.keys(errorMessages).length > 0) {
                c.status(400);
                return c.json(errorMessages);
            }
            try {
                await s3Service.deleteObject(bucketName, key);
                c.status(204);
                return c.text('Object Deleted Successfully');
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                if (e.name === 'NoSuchKey') {
                    c.status(404);
                    return c.text('Object Not Found');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })
        .get('/:bucketName/download', async (c) => {
            const {bucketName} = c.req.param();
            const {key} = c.req.query();
            const errorMessages = verifyQueryParams(c.req.query(), 'key');
            if (Object.keys(errorMessages).length > 0) {
                c.status(400);
                return c.json(errorMessages);
            }
            try {
                const data = await s3Service.downloadObject(bucketName, key);
                if (data.Body === undefined) {
                    c.status(404);
                    return c.text('Object has no content');
                }
                const s3Object = await data.Body.transformToByteArray();
                const filename = key.split('/').pop() ?? key;
                c.header('Content-type', 'application/octet-stream');
                c.header('Content-disposition', `attachment; filename=${encodeURI(filename)}`)
                return stream(c, async (stream) => {
                    stream.onAbort(() => {
                        console.log('Aborted')
                    })
                    await stream.write(s3Object)
                })
            } catch (e: any) {
                if (e.name === 'NoSuchBucket') {
                    c.status(404);
                    return c.text('Bucket Not Found');
                }
                if (e.name === 'NoSuchKey') {
                    c.status(404);
                    return c.text('Object Not Found');
                }
                c.status(500);
                return c.text('Internal Server Error');
            }
        })

    const api = new Hono();

    api
        .route('/bucket', bucket)
        .get('/listBuckets', async (c) => {
            const {prefix} = c.req.query();
            try {
                return c.json(await s3Service.listBuckets(prefix ?? ''));
            } catch (e) {
                console.error(e);
                return c.status(500);
            }
        })


    const app = new Hono()

    app
        .use(cors())
        .use('/static/*', serveStatic({root: './',}))
        .get('/*', serveStatic({root: './static'}))
        .use('/favicon.ico', serveStatic({path: './favicon.ico'}))
        .route('/api', api)
        .get('/ui', swaggerUI({url: '/doc'}))
        .get('/doc', (c) => {
            return c.json({
                openapi: '3.0.0',
                info: {
                    title: 'S3 Service',
                    version: '1.0.0',
                    description: 'S3 Service API'
                },
                paths: {
                    '/api/listBuckets': {
                        get: {
                            summary: 'List Buckets',
                            responses: {
                                '200': {
                                    description: 'List of buckets'
                                },
                            },
                            parameters: [
                                {
                                    name: 'prefix',
                                    in: 'query',
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                    '/api/bucket/{bucketName}': {
                        get: {
                            summary: 'List Objects in a Bucket',
                            responses: {
                                '200': {
                                    description: 'List of Objects in the bucket'
                                },
                                '404': {
                                    description: 'Bucket Not Found'
                                }
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                },
                                {
                                    name: 'prefix',
                                    in: 'query',
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        },
                    },
                    '/api/bucket/{bucketName}/create': {
                        post: {
                            summary: 'Create a Bucket',
                            responses: {
                                '200': {
                                    description: 'Bucket Created Successfully'
                                },
                                '409': {
                                    description: 'Bucket Already Exists or Invalid Bucket Name'
                                },
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        },
                    },
                    '/api/bucket/{bucketName}/delete': {
                        delete: {
                            summary: 'Delete a Bucket',
                            responses: {
                                '200': {
                                    description: 'Bucket Deleted Successfully'
                                },
                                '404': {
                                    description: 'Bucket Not Found'
                                },
                                '409': {
                                    description: 'Bucket Not Empty'
                                }
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                    '/api/bucket/{bucketName}/addObject': {
                        post: {
                            summary: 'Add an Object to a Bucket',
                            responses: {
                                '200': {
                                    description: 'Object Uploaded Successfully'
                                },
                                '404': {
                                    description: 'Bucket Not Found'
                                }
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                },
                                {
                                    name: 'objectName',
                                    in: 'query',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                    '/api/bucket/{bucketName}/addPrefix': {
                        post: {
                            summary: 'Add a Prefix to a Bucket',
                            responses: {
                                '200': {
                                    description: 'Prefix Added Successfully'
                                },
                                '404': {
                                    description: 'Bucket Not Found'
                                },
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                },
                                {
                                    name: 'prefix',
                                    in: 'query',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                    '/api/bucket/{bucketName}/deleteObject': {
                        delete: {
                            summary: 'Delete an Object from a Bucket',
                            responses: {
                                '200': {
                                    description: 'Object Deleted Successfully'
                                },
                                '404': {
                                    description: 'Object Not Found or Bucket Not Found'
                                }
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                },
                                {
                                    name: 'objectName',
                                    in: 'query',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                    '/api/bucket/{bucketName}/download': {
                        get: {
                            summary: 'Download an Object from a Bucket',
                            responses: {
                                '200': {
                                    description: 'Object Content'
                                },
                                '404': {
                                    description: 'Object Not Found or Bucket Not Found or Object has no content'
                                },
                            },
                            parameters: [
                                {
                                    name: 'bucketName',
                                    in: 'path',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                },
                                {
                                    name: 'objectName',
                                    in: 'query',
                                    required: true,
                                    schema: {
                                        type: 'string'
                                    }
                                }
                            ]
                        }
                    },
                }
            });
        })
        .get('*', serveStatic({path: './static/index.html'}))

    return app

}

export default build
