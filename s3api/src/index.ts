import S3Service from "./service/s3-service";
import build from "./app";
import EnvS3Config from "./config/EnvS3Config";

const s3Config = new EnvS3Config();
const s3Client = s3Config.getS3Client();
const s3Service = new S3Service(s3Client);

const app = build(s3Service)

export default app