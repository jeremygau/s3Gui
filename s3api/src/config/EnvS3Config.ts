import {S3Client} from "@aws-sdk/client-s3";

export default class EnvS3Config {
    forcePathStyle: boolean;
    endpoint: string;
    region: string;
    credentials: { accessKeyId: string, secretAccessKey: string };

    constructor() {
        this.forcePathStyle = Bun.env.FORCE_PATH_STYLE === 'true';
        this.endpoint = `${Bun.env.SCHEME ?? 'https'}://${Bun.env.ENDPOINT ?? 's3.amazonaws.com'}`;
        this.region = Bun.env.REGION ?? 'us-east-1';
        this.credentials = {
            accessKeyId: Bun.env.AWS_ACCESS_KEY_ID ?? '',
            secretAccessKey: Bun.env.AWS_SECRET_ACCESS_KEY ?? ''
        }
    }

    getS3Client(): S3Client {
        return new S3Client({
            forcePathStyle: this.forcePathStyle,
            endpoint: this.endpoint,
            region: this.region,
            credentials: this.credentials
        });
    }
}