type Credentials = {
    accessKeyId: string;
    secretAccessKey: string;
}