import {
    type Bucket,
    CreateBucketCommand,
    DeleteBucketCommand,
    DeleteObjectCommand,
    GetObjectCommand,
    ListBucketsCommand,
    ListObjectsCommand,
    PutObjectCommand,
    S3Client
} from "@aws-sdk/client-s3";
import S3Service from "../../src/service/s3-service";
import {describe, expect, it} from "bun:test";
import {mockClient} from "aws-sdk-client-mock";
import * as fs from "fs";
import {sdkStreamMixin} from "@aws-sdk/util-stream-node";

const mockS3Client = mockClient(S3Client);
const s3Service: S3Service = new S3Service(mockS3Client as unknown as S3Client);
describe('listBuckets', () => {

    it('should return a bucket list', async () => {
        const mockBuckets = [{Name: 'bucket1'}, {Name: 'bucket2'}];
        mockS3Client.on(ListBucketsCommand).resolves({Buckets: mockBuckets});

        const result = await s3Service.listBuckets();
        expect(result).toEqual(mockBuckets);
    });

    it('should return an empty list', async () => {
        const mockBuckets: Bucket[] = [];
        mockS3Client.on(ListBucketsCommand).resolves({Buckets: mockBuckets});

        const result = await s3Service.listBuckets();
        expect(result).toEqual(mockBuckets);
    });

    it('should throw an error', async () => {
        mockS3Client.on(ListBucketsCommand).rejects(new Error('Error'));
        expect(s3Service.listBuckets()).rejects.toThrow(new Error('Error'));
    });
});

describe('createBucket', () => {

    it('should create a bucket', async () => {
        mockS3Client.on(CreateBucketCommand).resolves({});
        await s3Service.createBucket('bucket1');
    });

    it('should throw an error', async () => {
        mockS3Client.on(CreateBucketCommand).rejects(new Error('Error'));
        expect(s3Service.createBucket('bucket1')).rejects.toThrow(new Error('Error'));
    });
});

describe('deleteBucket', () => {

    it('should delete a bucket', async () => {
        mockS3Client.on(DeleteBucketCommand).resolves({});
        await s3Service.deleteBucket('bucket1');
    });

    it('should throw an error', async () => {
        mockS3Client.on(DeleteBucketCommand).rejects(new Error('Error'));
        expect(s3Service.deleteBucket('bucket1')).rejects.toThrow(new Error('Error'));
    });
})

describe('listObjects', () => {

    it('should return an object list', async () => {
        mockS3Client.on(ListObjectsCommand).resolves({Contents: [{Key: 'object1'}, {Key: 'object2'}]});

        const result = await s3Service.listObjects('bucket1');
        expect(result).toEqual([{Key: 'object1'}, {Key: 'object2'}]);
    });

    it('should return an empty list', async () => {
        mockS3Client.on(ListObjectsCommand).resolves({Contents: []});

        const result = await s3Service.listObjects('bucket1');
        expect(result).toEqual([]);
    });

    it('should throw an error', async () => {
        mockS3Client.on(ListObjectsCommand).rejects(new Error('Error'));
        expect(s3Service.listObjects('bucket1')).rejects.toThrow(new Error('Error'));
    });
});

describe('addObject', () => {

    it('should add an object', async () => {
        mockS3Client.on(PutObjectCommand).resolves({});
        await s3Service.addObject('bucket1', 'prefix', 'file1', Buffer.from('content'));
    });

    it('should throw an error', async () => {
        mockS3Client.on(PutObjectCommand).rejects(new Error('Error'));
        expect(s3Service.addObject('bucket1', 'prefix', 'file1', Buffer.from('content'))).rejects.toThrow(new Error('Error'));
    });
});

describe('addPrefix', () => {

    it('should add a prefix', async () => {
        mockS3Client.on(PutObjectCommand).resolves({});
        await s3Service.addPrefix('bucket1', 'prefix');
    });

    it('should throw an error', async () => {
        mockS3Client.on(PutObjectCommand).rejects(new Error('Error'));
        expect(s3Service.addPrefix('bucket1', 'prefix')).rejects.toThrow(new Error('Error'));
    });
});

describe('deleteObject', () => {

    it('should delete an object', async () => {
        mockS3Client.on(DeleteObjectCommand).resolves({});
        await s3Service.deleteObject('bucket1', 'object1');
    });

    it('should throw an error', async () => {
        mockS3Client.on(DeleteObjectCommand).rejects(new Error('Error'));
        expect(s3Service.deleteObject('bucket1', 'object1')).rejects.toThrow(new Error('Error'));
    });
});

describe('downloadObject', () => {

    it('should download an object', async () => {
        const stream = sdkStreamMixin(fs.createReadStream(`${import.meta.dir}/${import.meta.file}`))
        mockS3Client.on(GetObjectCommand).resolves({
            Body: stream,
        });
        const res = await s3Service.downloadObject('bucket1', 'object1');
        expect(res.Body).toEqual(stream);
    });

    it('should throw an error', async () => {
        mockS3Client.on(GetObjectCommand).rejects(new Error('Error'));
        expect(s3Service.downloadObject('bucket1', 'object1')).rejects.toThrow(new Error('Error'));
    });
});