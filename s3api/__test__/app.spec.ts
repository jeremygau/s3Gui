import {describe, expect, it, spyOn} from "bun:test";

import build from "../src/app";

import S3Service from "../src/service/s3-service";
import {NoSuchBucket, NoSuchKey, S3Client} from "@aws-sdk/client-s3";
import {sdkStreamMixin} from '@aws-sdk/util-stream-node';
import * as fs from 'node:fs';

const s3Service = new S3Service(new S3Client())
const app = build(s3Service)

describe('GET /api/listBuckets', async () => {
    it('should return an empty array', async () => {
        spyOn(s3Service, 'listBuckets').mockResolvedValue([])

        const res = await app.request('/api/listBuckets', {
            method: 'GET',
        })
        expect(res.status).toBe(200)
        expect(await res.json()).toEqual([])
    })

    it('should return a list of buckets', async () => {
        const dateBucket1 = new Date();
        const dateBucket2 = new Date();
        const mockBuckets = [
            {Name: 'bucket1', CreationDate: dateBucket1},
            {Name: 'bucket2', CreationDate: dateBucket2},
        ];
        spyOn(s3Service, 'listBuckets').mockResolvedValue(mockBuckets)

        const res = await app.request('/api/listBuckets', {
            method: 'GET',
        })
        expect(res.status).toBe(200)
        expect(await res.json()).toEqual([
            {Name: 'bucket1', CreationDate: dateBucket1.toISOString()},
            {Name: 'bucket2', CreationDate: dateBucket2.toISOString()},
        ])
    })

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'listBuckets').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/listBuckets', {
            method: 'GET',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })
})

describe('GET /api/bucket/{bucketName}', async () => {
    it('should return a list of objects', async () => {
        const dateObject1 = new Date();
        const dateObject2 = new Date();
        const mockObjects = [
            {Key: 'object1', LastModified: dateObject1, Size: 1024},
            {Key: 'object2', LastModified: dateObject2, Size: 2048},
        ];
        spyOn(s3Service, 'listObjects').mockResolvedValue(mockObjects)

        const res = await app.request('/api/bucket/testBucket', {
            method: 'GET',
        })
        expect(res.status).toBe(200)
        expect(await res.json()).toEqual([
            {Key: 'object1', LastModified: dateObject1.toISOString(), Size: 1024},
            {Key: 'object2', LastModified: dateObject2.toISOString(), Size: 2048},
        ])
    })

    it('should return an 404 error', async () => {
        spyOn(s3Service, 'listObjects').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket', {
            method: 'GET',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    })

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'listObjects').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket', {
            method: 'GET',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })
})

describe('POST /api/bucket/{bucketName}/create', async () => {
    it('should return 201', async () => {
        spyOn(s3Service, 'createBucket').mockResolvedValue()

        const res = await app.request('/api/bucket/testBucket/create', {
            method: 'POST',
        })
        expect(res.status).toBe(201)
    })


    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'createBucket').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket/create', {
            method: 'POST',
            body: JSON.stringify({bucketName: 'bucket1'}),
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })
})

describe('DELETE /api/bucket/{bucketName}/delete', async () => {
    it('should return 204', async () => {
        spyOn(s3Service, 'deleteBucket').mockResolvedValue()

        const res = await app.request('/api/bucket/testBucket/delete', {
            method: 'DELETE',
        })
        expect(res.status).toBe(204)
    })

    it('should return an 404 error', async () => {
        spyOn(s3Service, 'deleteBucket').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/delete', {
            method: 'DELETE',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    })

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'deleteBucket').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket/delete', {
            method: 'DELETE',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })
})

describe('POST /api/bucket/{bucketName}/addObject', async () => {
    it('should return 201', async () => {
        spyOn(s3Service, 'addObject').mockResolvedValue()

        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject?prefix=&key=testFile', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(201)
    })

    it('should return 404', async () => {
        spyOn(s3Service, 'addObject').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject?prefix=&key=testFile', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    });

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'addObject').mockRejectedValue(new Error('error'))

        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject?prefix=&key=testFile', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })

    it('should return 400 prefix missing', async () => {
        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject?key=testFile', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({prefix: 'prefix is required'})
    });

    it('should return 400 key missing', async () => {
        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject?prefix=', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({key: 'key is required'})
    });

    it('should return 400 prefix and key missing', async () => {
        const form = new FormData();
        form.append('file', new File([], ""), 'testFile');

        const res = await app.request('/api/bucket/testBucket/addObject', {
            method: 'POST',
            body: form,
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({prefix: 'prefix is required', key: 'key is required'})
    });

    it('should return 400 file missing no form data', async () => {
        const res = await app.request('/api/bucket/testBucket/addObject?prefix=&key=testFile', {
            method: 'POST',
        })

        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({file: 'file is required'})
    });

    it('should return 400 file missing no file in form data', async () => {
        const res = await app.request('/api/bucket/testBucket/addObject?prefix=&key=testFile', {
            method: 'POST',
            body: new FormData(),
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({file: 'file is required'})
    });
})

describe('GET /api/bucket/{bucketName}/addPrefix', async () => {
    it('should return 201', async () => {
        spyOn(s3Service, 'addPrefix').mockResolvedValue()

        const res = await app.request('/api/bucket/testBucket/addPrefix?prefix=testPrefix', {
            method: 'POST',
        })
        expect(res.status).toBe(201)
    })

    it('should return 404', async () => {
        spyOn(s3Service, 'addPrefix').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/addPrefix?prefix=testPrefix', {
            method: 'POST',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    });

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'addPrefix').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket/addPrefix?prefix=testPrefix', {
            method: 'POST',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })

    it('should return 400 prefix missing', async () => {
        const res = await app.request('/api/bucket/testBucket/addPrefix', {
            method: 'POST',
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({prefix: 'prefix is required'})
    });
})

describe('DELETE /api/bucket/{bucketName}/delete', async () => {
    it('should return 204', async () => {
        spyOn(s3Service, 'deleteObject').mockResolvedValue()

        const res = await app.request('/api/bucket/testBucket/deleteObject?key=testFile', {
            method: 'DELETE',
        })
        expect(res.status).toBe(204)
    })

    it('should return 404 Bucket not found', async () => {
        spyOn(s3Service, 'deleteObject').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/deleteObject?key=testFile', {
            method: 'DELETE',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    });

    it('should return 404 Object not found', async () => {
        spyOn(s3Service, 'deleteObject').mockRejectedValue(new NoSuchKey({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/deleteObject?key=testFile', {
            method: 'DELETE',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Object Not Found')
    })

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'deleteObject').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket/deleteObject?key=testFile', {
            method: 'DELETE',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })

    it('should return 400 key missing', async () => {
        const res = await app.request('/api/bucket/testBucket/deleteObject', {
            method: 'DELETE',
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({key: 'key is required'})
    });
})

describe('GET /api/bucket/{bucketName}/download', async () => {
    it('should return 200', async () => {
        spyOn(s3Service, 'downloadObject').mockResolvedValue({
            Body: sdkStreamMixin(fs.createReadStream(`${import.meta.dir}/${import.meta.file}`)),
            $metadata: {}
        })

        const res = await app.request('/api/bucket/testBucket/download?key=testFile', {
            method: 'GET',
        })
        expect(res.status).toBe(200)
        expect(res.headers.get('content-disposition')).toBe('attachment; filename=testFile')
        expect(res.headers.get('content-type')).toBe('application/octet-stream')
        expect(await res.blob()).toEqual(Bun.file(`${import.meta.dir}/${import.meta.file}`))
    })

    it('should return 404 Object has no content', async () => {
        spyOn(s3Service, 'downloadObject').mockResolvedValue({Body: undefined, $metadata: {}})

        const res = await app.request('/api/bucket/testBucket/download?key=testFile', {
            method: 'GET',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Object has no content')
    })

    it('should return 404 Bucket not found', async () => {
        spyOn(s3Service, 'downloadObject').mockRejectedValue(new NoSuchBucket({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/download?key=testFile', {
            method: 'GET',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Bucket Not Found')
    });

    it('should return 404 Object not found', async () => {
        spyOn(s3Service, 'downloadObject').mockRejectedValue(new NoSuchKey({$metadata: {}, message: ""}))

        const res = await app.request('/api/bucket/testBucket/download?key=testFile', {
            method: 'GET',
        })
        expect(res.status).toBe(404)
        expect(await res.text()).toBe('Object Not Found')
    });

    it('should return an Internal Server Error', async () => {
        spyOn(s3Service, 'downloadObject').mockRejectedValue(new Error('error'))

        const res = await app.request('/api/bucket/testBucket/download?key=testFile', {
            method: 'GET',
        })
        expect(res.status).toBe(500)
        expect(await res.text()).toBe('Internal Server Error')
    })

    it('should return 400 key missing', async () => {
        const res = await app.request('/api/bucket/testBucket/download', {
            method: 'GET',
        })
        expect(res.status).toBe(400)
        expect(await res.json()).toStrictEqual({key: 'key is required'})
    });
})