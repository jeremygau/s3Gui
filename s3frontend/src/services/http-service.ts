async function request(url: string, options?: RequestInit): Promise<Response> {
    const response = await fetch(`${import.meta.env.VITE_IP_API}${url}`, options);
    if (!response.ok) {
        throw new Error(response.statusText);
    }
    return response;
}

export async function listBuckets(): Promise<Array<S3Bucket>> {
    const response = await request('/listBuckets');
    return response.json();
}

export async function createBucket(bucket: string) {
    return await request(`/bucket/${bucket}/create`, {
        method: 'POST',
    });
}

export async function deleteBucket(bucket: string) {
    return await request(`/bucket/${bucket}/delete`, {
        method: 'DELETE',
    });
}

export async function listObjects(bucket: string): Promise<Array<S3Object>> {
    const response = await request(`/bucket/${bucket}`);
    return response.json();
}

export async function uploadObject(bucket: string, prefix: string, file: File) {
    const formData = new FormData();
    formData.append('file', file);
    return await request(`/bucket/${bucket}/addObject?prefix=${prefix}&key=${encodeURI(file.name)}`, {
        method: 'POST',
        body: formData,
    });
}

export async function downloadObject(bucket: string, key: string) {
    const response = await request(`/bucket/${bucket}/download?key=${key}`);
    const blob = await response.blob();
    return URL.createObjectURL(blob);
}

export async function deleteObject(bucket: string, key: string) {
    return await request(`/bucket/${bucket}/deleteObject?key=${key}`, {
        method: 'DELETE',
    });
}

export async function addPrefix(bucketName: string, prefix: string) {
    return await request(`/bucket/${bucketName}/addPrefix?prefix=${prefix}`, {
        method: 'POST',
    });
}
