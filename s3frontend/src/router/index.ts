import {createRouter, createWebHistory} from 'vue-router';
import BucketsView from '@/views/BucketsView.vue';
import FilesView from '@/views/ObjectsView.vue';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            redirect: '/buckets',
        },
        {
            path: '/buckets',
            name: 'listBuckets',
            component: BucketsView,
        },
        {
            path: '/:bucketName',
            name: 'bucket',
            component: FilesView,
        },
    ],
});

export default router;
