type S3Bucket = {
    Name: string;
    CreationDate: string;
}

type S3Object = {
    Key: string;
    LastModified: string;
    ETag: string;
    Size: number;
    StorageClass: string;
    Owner: {
        DisplayName: string;
        ID: string;
    }
}
