FROM oven/bun:canary-alpine

COPY s3api/package.json ./
COPY s3api/bun.lockb ./

RUN bun install

COPY s3api/src ./
COPY s3frontend/dist ./static


CMD ["bun", "run", "index.ts"]
